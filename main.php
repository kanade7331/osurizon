<?php
if(!require('config.php')) {
	echo "config.php not found"; 
	exit;
}

$tpl->draw('header');

function checkAge( $uname ) {
	$filepath = $_SERVER['DOCUMENT_ROOT'] . '/cache/users/' . $uname . '.txt';
	if (!file_exists($filepath) or time()-filemtime($filepath) > 1 * 3600) {
		$done = file_get_contents('https://osu.ppy.sh/api/get_user?k=' . APIKEY . '&u=' . $uname );
		$file = fopen($filepath, "w");
		fwrite($file, $done);
		fclose($file); 
	} else {
		$done = file_get_contents( $filepath );
	}
	return $done;
}

function doShit( $uname ) {
	global $tpl;
	$stuff =  json_decode(checkAge($uname), true);
	$osuentries = array();
	foreach ( $stuff as $event) 
	{
		array_push($osuentries, array(
			"avatar" => $event['user_id'],
			"username" => $event['username'], // added
			"playcount" => $event['playcount'], //added
			"pp_rank" => $event['pp_rank'], //added
			"level" => $event['level'], //Added
			"ranked_score" => number_format($event['ranked_score'], 0, ' ', ' '), //added
			"accuracy" => number_format($event['accuracy'], 2) //added
			));
		$tpl->assign("osuentries", $osuentries);
		$tpl->draw('onebox');
	}
}

foreach ( $osuers as $player ) {
	doShit($player);
}

$tpl->draw('footer');
?>