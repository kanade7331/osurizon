<?php
if(!require('config.php')) {
	echo "config.php not found"; 
	exit;
}


$tpl->draw('header');

function checkAge( $uname ) {
	$filepath = $_SERVER['DOCUMENT_ROOT'] . '/cache/users/' . $uname . '.txt';
	if (!file_exists($filepath) or time()-filemtime($filepath) > 1 * 3600) {
		$done = file_get_contents('https://osu.ppy.sh/api/get_user?k=' . APIKEY . '&u=' . $uname );
		$file = fopen($filepath, "w");
		fwrite($file, $done);
		fclose($file); 
	} else {
		$done = file_get_contents( $filepath );
	}
	return $done;
}

function checkTime($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : '';
}

function doShit( $uname ) {
	global $tpl;
	$stuff =  json_decode(checkAge($uname), true);
	$recentstuff = array();
	$osuentries = array();
	foreach ( $stuff as $event) 
	{
		array_push($osuentries, array(
			"avatar" => $event['user_id'],
			"username" => $event['username'], // added
			"playcount" => $event['playcount'], //added
			"pp_rank" => $event['pp_rank'], //added
			"level" => $event['level'], //Added
			"ranked_score" => number_format($event['ranked_score'], 0, ' ', ' '), //added
			"accuracy" => number_format($event['accuracy'], 2) //added
			));
		$tpl->assign("osuentries", $osuentries);
		$tpl->draw('onebox');
	}
	for ( $i = 0; $i < 6; ++$i ) {
		$toreplace = $stuff[0]['events'][$i]['display_html'];
		$replaced = str_replace("<img src='/images/","<img src='//osu.ppy.sh/images/", $toreplace);
		$replacedbeforelast = str_replace("<a href='/b/","<a href='//osu.ppy.sh/b/", $replaced);
		$replacedlast = str_replace("<a href='/u/","<a href='//osu.ppy.sh/u/", $replacedbeforelast);
		array_push($osuentries, array(
			"event" => $replacedlast,
			"time" => checkTime($stuff[0]['events'][$i]['date'])
			));
		$tpl->assign("events", $osuentries);
	}
	foreach ( $stuff as $event) 
	{
		array_push($recentstuff, array(
			"avatar" => $event['user_id'],
			"username" => $event['username']
			));
		$tpl->assign("recentstuff", $recentstuff);
		$tpl->draw('statsbox');
	}
}

doShit($_GET['u']);

$tpl->draw('footer');
?>